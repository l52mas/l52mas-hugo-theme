---
name: "{{ replace .Name "-" " " | name }}"
twitter: "@twitter"
img: URL_to_gravatar
role:
    es: "Estudiante"
    en: "Student"
active: true
now:
    es: "Si ya estás en otro lado sino borralo"
    en: "If you are in other place otherwise erase"
date: {{ .Date }}
status: active
---
